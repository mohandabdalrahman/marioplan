import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

var config = {
  apiKey: "AIzaSyBGC0e4jR16B7v2Deka5tG-FunAJRla_20",
  authDomain: "marioplan-4d8fe.firebaseapp.com",
  databaseURL: "https://marioplan-4d8fe.firebaseio.com",
  projectId: "marioplan-4d8fe",
  storageBucket: "marioplan-4d8fe.appspot.com",
  messagingSenderId: "816371935293"
};
firebase.initializeApp(config);

firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
