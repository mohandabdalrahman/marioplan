export const createProject = project => {
  return (dispatch, getState, { getFirestore, getFirebase }) => {
    // know which project connect
    // async call

    const fireStore = getFirestore();
    const profile = getState().firebase.profile;
    const authId = getState().firebase.auth.uid;
    fireStore
      .collection("projects")
      .add({
        // add new document
        ...project,
        authFirstName: "mohand",
        authLastName: "abdalrahman",
        authorId: 1234,
        createAd: new Date()
      })
      .then(() => {
        dispatch({ type: "CREATE_PROJECT", project });
      })
      .catch(err => {
        dispatch({ type: "CREATE_PROJECT_ERR", err });
      });
  };
};
