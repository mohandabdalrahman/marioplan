const initstate = {
  projects: [
    { id: 1, title: "blah blah blah", content: "blah blah blah" },
    { id: 2, title: "blah blah blah", content: "blah blah blah" },
    { id: 3, title: "blah blah blah", content: "blah blah blah" }
  ]
};

const projectReducer = (state = initstate, action) => {
  switch (action.type) {
    case "CREATE_PROJECT":
      console.log("create project", action.project);
      return state;
    case "CREATE_PROJECT_ERR":
      console.log("project error", action.err);
      return state;
    default:
      return state;
  }
};

export default projectReducer;
