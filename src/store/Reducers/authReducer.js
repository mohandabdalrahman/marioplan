const initstate = {
  authError: null
};

const authReducer = (state = initstate, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      console.log("login");
      return {
        ...state,
        authError: "login_failed"
      };

    case "LOGIN_ERROR":
      return {
        ...state,
        authError: null
      };
    case "SIGNOUT_SUCSESS":
      console.log("signout");
      return state;
    case "SIGNUP_SUCCESS":
      console.log("signup");
      return {
        ...state,
        authError: null
      };
    case "SIGNUP_ERR":
      console.log("signup error");
      return {
        ...state,
        authError: action.err.message
      };
    default:
      return state;
  }
};

export default authReducer;
